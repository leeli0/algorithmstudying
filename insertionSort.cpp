#include <iostream>
using namespace std;

void insertionSort(int arr[], int length) {
    int i, j, current;
    
    for (i = 1; i < length; i++) {
        current = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > current) {
            arr[j + 1] = arr[j];
            j--;
        }

        arr[j + 1] = current;
    }
}

int main() {
    int arr[] = {5, 2, 9, 1, 3};
    int length = sizeof(arr) / sizeof(arr[0]);
    
    insertionSort(arr, length);
    
    cout << "Sorted array: ";
    for (int i = 0; i < length; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    
    return 0;
}
