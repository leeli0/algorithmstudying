function insertionSort(arr) {
    var length = arr.length;
    for (var i = 1; i < length; i++) {
        var current = arr[i];
        var j = i - 1;
        while (j >= 0 && arr[j] > current) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = current;
    }
    return arr;
}
// Example usage:
var unsortedArray = [5, 2, 9, 1, 3];
var sortedArray = insertionSort(unsortedArray);
console.log(sortedArray); // Output: [1, 2, 3, 5, 9]
