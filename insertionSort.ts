function insertionSort(arr: number[]): number[] {
    const length = arr.length;
    
    for (let i = 1; i < length; i++) {
      const current = arr[i];
      let j = i - 1;
  
      while (j >= 0 && arr[j] > current) {
        arr[j + 1] = arr[j];
        j--;
      }
  
      arr[j + 1] = current;
    }
  
    return arr;
  }
  
  // Example usage:
  const unsortedArray = [5, 2, 9, 1, 3];
  const sortedArray = insertionSort(unsortedArray);
  console.log(sortedArray); // Output: [1, 2, 3, 5, 9]
  