fn insertion_sort(arr: &mut [i32]) {
    let n = arr.len();

    for i in 1..n {
        let current = arr[i];
        let mut j = i as i32 - 1;

        while j >= 0 && arr[j as usize] > current {
            arr[(j + 1) as usize] = arr[j as usize];
            j -= 1;
        }

        arr[(j + 1) as usize] = current;
    }
}

fn main() {
    let mut arr = [5, 2, 9, 1, 3];

    insertion_sort(&mut arr);

    println!("Sorted array: {:?}", arr);
}
