def insertion_sort(arr):
    n = len(arr)

    for i in range(1, n):
        current = arr[i]
        j = i - 1

        while j >= 0 and arr[j] > current:
            arr[j + 1] = arr[j]
            j -= 1

        arr[j + 1] = current

arr = [5, 2, 9, 1, 3]
insertion_sort(arr)

print("Sorted array:", arr)
